package cn.itcast.travel.web.servlet;

import cn.itcast.travel.dao.RouteDao;
import cn.itcast.travel.dao.impl.RouteDaoImpl;
import cn.itcast.travel.domain.PageBean;

public class RouteServiceImpl implements RouteService {
    RouteDao routeDao = new RouteDaoImpl();
    @Override
    public PageBean findPage(int cid, int currentPage, int pageSize) {
        PageBean pb = new PageBean();
        pb.setCurrentPage(currentPage);
        pb.setPageSize(pageSize);

        int totalCount = routeDao.totalCount(cid);
        pb.setTotalCount(totalCount);//总条数
        //计算总页数  500  5 = 100    503   5  = 101
        int totalPage = (totalCount%pageSize==0)?(totalCount/pageSize):(totalCount/pageSize+1);
        pb.setTotalPage(totalPage);

        //起始的索引位置
        //第1磁数据：数据库里要查询的是第0每次数据，第一条数据的起始索引是0   查5条   0，1，2，3，4
        //第2页数据：数据库里要查询的是第一条数据，第一条数据的起始索引是5    查5条   5，6，7，8，9
        //第3页数据：数据库里要查询的是第二条数据，第一条数据的起始索引是10    查5条   10，11，12，13，14

        int start = (currentPage-1)*pageSize;
        pb.setList(routeDao.findByCid(cid,start,pageSize));

        return pb;
    }
}
