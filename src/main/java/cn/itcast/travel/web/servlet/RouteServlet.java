package cn.itcast.travel.web.servlet;

import cn.itcast.travel.domain.Category;
import cn.itcast.travel.domain.PageBean;
import cn.itcast.travel.service.CategoryService;
import cn.itcast.travel.service.impl.CategoryServiceImpl;
import cn.itcast.travel.util.JedisUtil;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Tuple;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@WebServlet("/route/*")
public class RouteServlet extends BaseServlet {
    RouteService routeService = new RouteServiceImpl();

    public void findPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, InvocationTargetException, IllegalAccessException {
        //获取cid
        String cidStr = request.getParameter("cid");
        //获取currentPage
        String currentPageStr = request.getParameter("currentPage");

        //把它们转成int类型
        int cid = 0;
        if (cidStr != null && !"".equalsIgnoreCase(cidStr) && !"null".equalsIgnoreCase(cidStr)) {
            cid = Integer.parseInt(cidStr);
        }

        int currentPage = 0;
        if (currentPageStr != null && !"".equalsIgnoreCase(currentPageStr) && !"null".equalsIgnoreCase(currentPageStr)) {
            currentPage = Integer.parseInt(currentPageStr);
        }

        //默认的pageSize
        int pageSize = 5;

        PageBean pb = routeService.findPage(cid,currentPage,pageSize);

        System.out.println(pb);
        writeValue(response,pb);

    }

}
