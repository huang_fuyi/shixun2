package cn.itcast.travel.web.servlet;

import cn.itcast.travel.domain.PageBean;

public interface RouteService {
    PageBean findPage(int cid, int currentPage, int pageSize);
}
