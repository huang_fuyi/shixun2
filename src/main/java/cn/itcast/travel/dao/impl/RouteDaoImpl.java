package cn.itcast.travel.dao.impl;

import cn.itcast.travel.dao.RouteDao;
import cn.itcast.travel.domain.Route;
import cn.itcast.travel.util.JDBCUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class RouteDaoImpl implements RouteDao {
    JdbcTemplate jt = new JdbcTemplate(JDBCUtils.getDataSource());
    @Override
    public int totalCount(int cid) {

        return jt.queryForObject("select count(*) from tab_route where cid=?",
                Integer.class,cid);
    }

    @Override
    public List<Route> findByCid(int cid, int start, int pageSize) {
        //limit ?,?:
        //第1个问号：开始的索引
        //第2个问题：查询的条数
        return jt.query("select * from tab_route where cid=? limit ?,?",
                new BeanPropertyRowMapper<Route>(Route.class),cid,start,pageSize);
    }
}
