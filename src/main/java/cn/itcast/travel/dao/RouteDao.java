package cn.itcast.travel.dao;

import cn.itcast.travel.domain.Route;

import java.util.List;

public interface RouteDao {
    int totalCount(int cid);

    List<Route> findByCid(int cid, int start, int pageSize);
}
